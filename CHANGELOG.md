# Changelog

## [0.3.0] - 2025-02-09

### Al49

- Support CUSTOM_VALID_OPTIONS env variable

### Apkbuild

- Add keepdirs options

### Apkbuild-lint

- Assume filename is APKBUILD if none is provided

### Deps

- Update golang.org/x/exp digest to b2144cd
- Update module github.com/stretchr/testify to v1.10.0

### Linter

- Support providing env function to checkers

## [0.2.1] - 2024-10-12

### Apkbuild

- Add maintainer as official variable

### Ci

- Switch to version image tag for golang

### Deps

- Update golang.org/x/exp digest to 9b4947d
- Update golang.org/x/exp digest to 701f63a
- Update module mvdan.cc/sh/v3 to v3.9.0

## [0.2.0] - 2024-08-25

### Al64

- Check for '[[' bashism
- Check for '==' bashism
- Check for 'read -p' bashism
- Check for 'type -p' bashism
- Check for '&>' bashism
- Check for 'source' bashism

### Apkbuild

- Capitalize testname

### Git

- Ignore built binary file

### Linter

- Add function to visit all statements
- Make sure all constants have an explicit type
- Omit explicit boolean comparisson
- Remove redundant fmt.Sprintf

## [0.1.1] - 2024-06-29

### Al36

- Support braced variables

### Apkbuild

- Move variable related code to dedicated file
- Add method to provide param expressions

### Deps

- Update module github.com/moznion/go-optional to v0.12.0

## [0.1.0] - 2024-06-01

### Al07

- Don't trigger for contents for heredocs

### Al1

- Do not trigger on parameter substitution

### Al11

- Don't trigger for single-line functions

### Al13

- Only check built-in functions

### Al31

- Provide the name of the offending variable
- Ignore built-in shell variables

### Al32

- Do not trigger on parameter expansions
- Do not trigger when succeeded by number
- Ignore parameter expansions with slices as well

### Al36

- Make an exception for perl modules

### Al5

- Do not trigger on custom variables

### Apkbuild

- Extract from pkg/atools
- Extract function for variable assignment
- Add more checks to TestParserFindGlobalVariables
- Support exported variables
- Add list of valid options
- Add loongarch64 to the list of supported arches

### Apkbuild-lint

- Rename from atools
- Return exit code 1 on issues

### Ast

- Command to print an AST of a file
- Support reading from stdin

### Atools

- Add basic structure for linters
- Add acceptance tests

### Atools-go

- Add license

### Atools_test

- Extract from pkg
- Test a full good example
- Remove AL15
- Extract function for no violations
- Verify AL32 does not target values in single quotes

### Build

- Improve configure script
- Add install redo file

### Ci

- Add verify stage
- Build on master
- Run configure in build step
- Move .gitlab-ci.yml to root

### Deps

- Upgrade mvdan.cc/sh/v3 to v3.7.0
- Add renovate.json
- Update golang.org/x/exp digest to c7f7c64
- Update golang.org/x/exp digest to c0f41cb
- Update golang.org/x/exp digest to 93d18d7
- Update golang.org/x/exp digest to fe59bbe
- Update golang.org/x/exp digest to 9bf2ced
- Update module gitlab.alpinelinux.org/alpine/go to v0.10.1
- Update golang.org/x/exp digest to 23cca88
- Update golang.org/x/exp digest to fd00a4e

### Go.mod

- Increase minimum go version to 1.20
- Remove invalid replacement

### Iterator

- Add new package

### Linter

- Make the linting output compliant
- Add method to instantiate linting violations
- Allow providing detail fields for LintingViolation
- Add function to sort violations by line
- Extract from pkg
- Handle exported variables
- Check for unnecessary return 1 (AL02)
- Check for function keyword (AL09)
- Check for double underscore vars in functions (AL30)
- Check for space after function name (AL10)
- Simplify check for al10
- Verify single space after function parenthesis (AL11)
- Check for newlines after function parenthesis (AL12)
- Check for superfluous cd builddir (al13)
- Check for quoted literal integers (al28)
- Fix option chmod-clean (AL49)
- Check for missing default_prepare (AL54)
- Check for invalid pkgver values (AL61)
- Add unit tests for AL32 not flaged in single quotes
- Remove stray debug print
- Use AST for CheckLiteralIntegerIsQuoted
- Add unittest for SuperfluousCDBuilddir
- Extract function to check for parameter substitution
- Check for parameter substitutions in double quotes
- Allow 'return 1' in block (AL2)

### Linter/al11

- Handle function lines without brackets

### Linters

- Move to a separate package
- Provide linting violation together with checker
- Allow skipping linters with env variables
- Check for use of space for indentation (AL7)
- Check for trailing whitespace
- Check for use of volatile source (AL62)
- Check for invalid options (AL49)
- Check for backticks usage (AL25)
- Adjust relavant violations to look at all assigns
- Check for invalid arches (AL57)
- Allow 'arch' to be empty (AL5)

### Linters/al35

- Handle deprecated variables

### Linters/al5

- Don't assume all variables have values
- Add missing return statement

### Lintes/variables_test

- Switch to using testify for assertions

### Parser

- Extract global variables
- Evaluate global variables
- Refactor global variables into map
- Add method to retrieve global variable
- Handle empty variables
- Ignore single command global variables
- Expose the lines of the parsed file
- Define an API for a variable scope
- Provide an iterator for variables
- Use a slice to store variables
- Provide statements instead of file.
- Extract variables from if-statements
- Extract global variables from case clauses
- Extract variables from for statements
- Extract variables from while statement
- Extract variables from brace blocks
- Rename parseGlobalVariables
- Support extracting functions
- Extract command substitutions in assignments
- Simplify iterators
- Provide iterator for all assignments
- Provide the statements for each function
- Explicitly set bash as variant
- Support local variables
- Add parser for call expressions
- Provide iterators for functions and statemnts
- Extract heredocs
- Extract heredocs from more contexts
- Use sh/expand to evaluate variables

### Parser_test

- Set example filename
- Use testify for assertions

### Ranges

- Add new package

### Variables_test

- Extract a function for test scaffolding

### Violations

- Add check for default builddir (AL1)
- Add check for quoted pkgname (AL3)
- Add check for quoted pkgver (AL4)
- Add check for empty variables (AL5)
- Check for use of custom variables (AL6)
- Add check for uppercase in pkgname (AL14)
- Add check for '-r' in pkgver (AL15)
- Check for '_builddir' without 'builddir' (AL26)
- Check for pkgname in source url (AL29)
- Check for variables with double underscore (AL30)
- Check for variables with capitals (AL31)
- Check for unnecessary braces (AL32)
- Check for cpan related variables (AL35)
- Check for overwriting xflags (AL36)


