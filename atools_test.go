package atools_test

import (
	"os"
	"path"
	"testing"

	"alpinelinux.org/go/atools/linter"
	"github.com/stretchr/testify/require"
)

func AccTest(t *testing.T, testFile string, violationCode string) {
	t.Helper()

	f, err := os.Open(path.Join("testdata", testFile))
	require.NoError(t, err, "could not open testdata/%s", testFile)

	violations := linter.Check(f, testFile)
	require.Len(t, violations, 1)
	require.Equal(t, violationCode, violations[0].Code)
}

func AccTestNoViolation(t *testing.T, testFile, description string) {
	f, err := os.Open(path.Join("testdata", testFile))
	require.NoError(t, err, "could not open testdata/%s", testFile)

	violations := linter.Check(f, testFile)
	require.Len(t, violations, 0, description)
}

func TestFullGoodExample(t *testing.T) {
	AccTestNoViolation(t, "full_good_example.sh", "No violations expected in the full example")
}

func TestAccAL1DefaultBuilddirVariable(t *testing.T) {
	AccTest(t, "al01_default_builddir_value.sh", "AL1")
}

func TestAccAL1DefaultBuilddirCorrectWithExpansion(t *testing.T) {
	AccTestNoViolation(
		t,
		"al01_default_builddir_correct_with_expansion.sh",
		"Expansions change the final builddir and should be taken into account",
	)
}

func TestAccAL1DefaultBuilddirCorrectReplace(t *testing.T) {
	AccTestNoViolation(
		t,
		"al01_default_builddir_correct_with_expansion.sh",
		"Replacements are can be necessary for certain versions only",
	)
}

func TestAccAL2UnnecessaryReturn1(t *testing.T) {
	AccTest(t, "al02_unnecessary_return_1.sh", "AL2")
}

func TestAccAL3PkgnameQuoted(t *testing.T) {
	AccTest(t, "al03_pkgname_quoted.sh", "AL3")
}

func TestAccAL4PkgverQuoted(t *testing.T) {
	AccTest(t, "al04_pkgver_quoted.sh", "AL4")
}

func TestAccAL5EmptyVariable(t *testing.T) {
	AccTest(t, "al05_empty_variable.sh", "AL5")
}

func TestAccAL5EmptyVariableCorrectArch(t *testing.T) {
	AccTestNoViolation(
		t,
		"al05_empty_variable_correct_arch.sh",
		"arch has a default value, so making it empty is sometimes necessary",
	)
}

func TestAccAL6CustomVariable(t *testing.T) {
	AccTest(t, "al06_custom_variable.sh", "AL6")
}

func TestAccAL7IndentTabs(t *testing.T) {
	AccTest(t, "al07_indent_tabs.sh", "AL7")
}

func TestAccAL7IndentTabsHeredocs(t *testing.T) {
	AccTestNoViolation(
		t,
		"al07_indent_tabs_heredocs.sh",
		"AL07 should not trigger for spaces in heredocs",
	)
}

func TestAccAL8TrailingWhitespace(t *testing.T) {
	AccTest(t, "al08_trailing_whitespace.sh", "AL8")
}

func TestAccAL9FunctionKeyword(t *testing.T) {
	AccTest(t, "al09_function_keyword.sh", "AL9")
}

func TestAccAL10SpaceBeforeFunctionParenthesis(t *testing.T) {
	AccTest(t, "al10_space_before_function_parenthesis.sh", "AL10")
}

func TestAccAL11SpaceAfterFunctionParenthesis(t *testing.T) {
	AccTest(t, "al11_space_after_function_parenthesis.sh", "AL11")
}

func TestAccAL12NewlineOpeningBrace(t *testing.T) {
	AccTest(t, "al12_newline_opening_brace.sh", "AL12")
}

func TestAccAL13SuperfluousCdBuilddir(t *testing.T) {
	AccTest(t, "al13_superfluous_cd_builddir.sh", "AL13")
}

func TestAccAL13SuperfluousCdBuilddirCorrect(t *testing.T) {
	AccTestNoViolation(t, "al13_superfluous_cd_builddir_correct.sh", "AL13 should not trigger when it's not the first cd")
}

func TestAccAL13SuperfluousCdBuilddirCustomFunction(t *testing.T) {
	AccTestNoViolation(t, "al13_superfluous_cd_builddir_custom_functions.sh", "AL13 should not trigger for custom functions")
}

func TestAccAL14PkgnameHasUppercase(t *testing.T) {
	AccTest(t, "al14_pkgname_has_uppercase.sh", "AL14")
}

func TestAccAL25BackticksUsage(t *testing.T) {
	AccTest(t, "al25_backticks_usage.sh", "AL25")
}

func TestAccAL26UnderBuilddirIsSet(t *testing.T) {
	AccTest(t, "al26_under_builddir_is_set.sh", "AL26")
}

func TestAccAL28LiteralIntegerIsQuoted(t *testing.T) {
	AccTest(t, "al28_literal_integer_is_quoted.sh", "AL28")
}

func TestAccAL29PkgnameUsedInSource(t *testing.T) {
	AccTest(t, "al29_pkgname_used_in_source.sh", "AL29")
}

func TestAccAL30DoubleUnderscoreInvariable(t *testing.T) {
	AccTest(t, "al30_double_underscore_in_variable.sh", "AL30")
}

func TestAccAL30DoubleUnderscoreInvariableFunc(t *testing.T) {
	AccTest(t, "al30_double_underscore_in_variable_func.sh", "AL30")
}

func TestAccAL30DoubleUnderscoreInvariableLocal(t *testing.T) {
	AccTest(t, "al30_double_underscore_in_variable_local.sh", "AL30")
}

func TestAccAL31VariableCapitalized(t *testing.T) {
	AccTest(t, "al31_variable_capitalized.sh", "AL31")
}

func TestAccAL32BracedVariable(t *testing.T) {
	AccTest(t, "al32_braced_variable.sh", "AL32")
}

func TestAccAL32BracedVariableLiteral(t *testing.T) {
	AccTestNoViolation(t, "al32_braced_variable_literal.sh", "Strings in single quotes should be ignored")
}

func TestAccAL35CPANVariableCPANDepends(t *testing.T) {
	AccTest(t, "al35_cpan_variable_cpandepends.sh", "AL35")
}

func TestAccAL35CPANVariableCPANMakeDepends(t *testing.T) {
	AccTest(t, "al35_cpan_variable_cpanmakedepends.sh", "AL35")
}

func TestAccAL35CPANVariableCPANCheckDepends(t *testing.T) {
	AccTest(t, "al35_cpan_variable_cpancheckdepends.sh", "AL35")
}

func TestAccAL36OverwriteXflagsCflags(t *testing.T) {
	AccTest(t, "al36_overwrite_xflags_cflags.sh", "AL36")
}

func TestAccAL36OverwriteXflagsCppflags(t *testing.T) {
	AccTest(t, "al36_overwrite_xflags_cppflags.sh", "AL36")
}

func TestAccAL36OverwriteXflagsCxxflags(t *testing.T) {
	AccTest(t, "al36_overwrite_xflags_cxxflags.sh", "AL36")
}

func TestAccAL36OverwriteXflagsGoflags(t *testing.T) {
	AccTest(t, "al36_overwrite_xflags_goflags.sh", "AL36")
}

func TestAccAL36OverwriteXflagsCflagsCorrect(t *testing.T) {
	AccTestNoViolation(
		t,
		"al36_overwrite_xflags_cflags_correct.sh",
		"AL36 should not trigger when the variable is present in the value (extending it)",
	)
}

func TestAccAL36OverwriteXflagsCflagsCorrectExpansion(t *testing.T) {
	AccTestNoViolation(
		t,
		"al36_overwrite_xflags_cflags_correct_expansion.sh",
		"AL36 should not trigger when the variable is present but is using parameter substitutions",
	)
}

func TestAccAL36OverwriteXflagsCflagsExceptPerl(t *testing.T) {
	testFile := "al36_overwrite_xflags_except_perl.sh"
	f, err := os.Open(path.Join("testdata", testFile))
	require.NoError(t, err, "could not open testdata/%s", testFile)

	violations := linter.Check(f, testFile)
	require.Len(t, violations, 0, "AL36 should not trigger for perl modules")
}

func TestAccAL49InvalidOption(t *testing.T) {
	AccTest(t, "al49_invalid_option.sh", "AL49")
}

func TestAccAL54MissingDefaultPrepare(t *testing.T) {
	AccTest(t, "al54_missing_default_prepare.sh", "AL54")
}

func TestAccAL57InvalidArch(t *testing.T) {
	AccTest(t, "al57_invalid_arch.sh", "AL57")
}

func TestAccAL61BadVersion(t *testing.T) {
	AccTest(t, "al61_bad_version.sh", "AL61")
}

func TestAccAL62VolatileSource(t *testing.T) {
	AccTest(t, "al62_volatile_source.sh", "AL62")
}

func TestAL64Bashism(t *testing.T) {
	t.Run("DoubleBracket", func(t *testing.T) {
		AccTest(t, "al64_not_approved_bashism_double_bracket.sh", "AL64")
	})
	t.Run("DoubleEquals", func(t *testing.T) {
		AccTest(t, "al64_not_approved_bashism_double_equals.sh", "AL64")
	})
	t.Run("ReadDashP", func(t *testing.T) {
		AccTest(t, "al64_not_approved_bashism_read_p.sh", "AL64")
	})
	t.Run("TypeDashP", func(t *testing.T) {
		AccTest(t, "al64_not_approved_bashism_read_p.sh", "AL64")
	})
	t.Run("RedirectAll", func(t *testing.T) {
		AccTest(t, "al64_not_approved_bashism_redirect.sh", "AL64")
	})
	t.Run("Source", func(t *testing.T) {
		AccTest(t, "al64_not_approved_bashism_source.sh", "AL64")
	})
}
