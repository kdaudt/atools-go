package linter

import (
	"testing"

	"alpinelinux.org/go/atools/apkbuild"
	"github.com/MakeNowJust/heredoc/v2"
	"github.com/stretchr/testify/require"
)

func WhitespaceTestCaseSingleViolation(
	testcase string,
	t *testing.T,
	checkFunc func(*apkbuild.Apkbuild, func(string) string) []LintingViolation,
	expectedViolation LintingViolation,
) {
	t.Helper()
	file := apkbuild.MakeFile(heredoc.Doc(testcase))

	apkbuild, err := apkbuild.Parse(file, "APKBUILD")
	require.NoError(t, err)

	violations := checkFunc(apkbuild, testEnv{}.Getenv)

	require.NotEmpty(t, violations, "expted at least violation %s", expectedViolation.Code)
	require.True(t, violations[0].Is(expectedViolation), "expected violation %s, got %s", expectedViolation.Code, violations[0].Code)
}

func WhitespaceTestCaseNoViolation(
	testcase string,
	t *testing.T,
	checkFunc func(*apkbuild.Apkbuild, func(string) string) []LintingViolation,
) {
	t.Helper()
	file := apkbuild.MakeFile(heredoc.Doc(testcase))

	apkbuild, err := apkbuild.Parse(file, "APKBUILD")
	require.NoError(t, err)

	violations := checkFunc(apkbuild, testEnv{}.Getenv)

	require.Empty(t, violations, "expected no violations")
}

func TestIndentTabs(t *testing.T) {
	t.Run("Normal", func(t *testing.T) {
		program := `
		pkgname=my-pkg
		pkgver=1.2.3
		build() {
		    make
		}
		`
		WhitespaceTestCaseSingleViolation(program, t, CheckIndentTabs, AL7)
	})
	t.Run("Heredoc", func(t *testing.T) {
		program := `
		pkgname=my-pkg
		pkgver=1.2.3
		build() {
		    cat -EOF
			message
			EOF
		}
		`
		WhitespaceTestCaseSingleViolation(program, t, CheckIndentTabs, AL7)
	})
	t.Run("EmptyHeredoc", func(t *testing.T) {
		program := `
		pkgname=my-pkg
		pkgver=1.2.3
		build() {
		    cat -EOF
			EOF
		}
		`
		WhitespaceTestCaseSingleViolation(program, t, CheckIndentTabs, AL7)
	})
}

func TestIndentTabsIgnoresHeredocs(t *testing.T) {
	t.Run("GlobalScope", func(t *testing.T) {
		program := heredoc.Doc(`
		pkgname=my-package
		cat <<EOF
		This is is some text
		  which uses spaces for indentation
		EOF
		`)

		WhitespaceTestCaseNoViolation(program, t, CheckIndentTabs)
	})
	t.Run("InFunction", func(t *testing.T) {
		program := heredoc.Doc(`
		pkgname=my-package
		prepare() {
			cat <<EOF
		This is is some text
		  which uses spaces for indentation
		EOF
		}
		`)

		WhitespaceTestCaseNoViolation(program, t, CheckIndentTabs)
	})
}

func TestTrailingWhitespace(t *testing.T) {
	program := `pkgname=my-pkg `

	WhitespaceTestCaseSingleViolation(program, t, CheckTrailingWhitespace, AL8)
}
