package linter

import (
	"fmt"
	"testing"

	"alpinelinux.org/go/atools/apkbuild"
	"github.com/MakeNowJust/heredoc/v2"
	"github.com/stretchr/testify/require"
)

func VariableTestCaseSingleViolation(
	testcase string,
	t *testing.T,
	checkFunc func(*apkbuild.Apkbuild, func(string) string) []LintingViolation,
	expectedViolation LintingViolation,
	env testEnv,
) {
	t.Helper()

	file := apkbuild.MakeFile(heredoc.Doc(testcase))

	apkbuild, err := apkbuild.Parse(file, "APKBUILD")
	require.NoError(t, err)

	violations := checkFunc(apkbuild, env.Getenv)

	require.NotEmpty(t, violations, "there should at least be one violation")
	require.True(t, violations[0].Is(expectedViolation), "expected violation %s, got %s", expectedViolation.Code, violations[0].Code)
}

func VariableTestCaseNoViolations(
	testcase string,
	t *testing.T,
	checkFunc func(*apkbuild.Apkbuild, func(string) string) []LintingViolation,
	env testEnv,
) {
	t.Helper()
	file := apkbuild.MakeFile(heredoc.Doc(testcase))

	apkbuild, err := apkbuild.Parse(file, "APKBUILD")
	require.NoError(t, err)

	violations := checkFunc(apkbuild, env.Getenv)

	require.Empty(t, violations, "unexpected violations received: %#v", violations)
}

func TestDefaultBuilddirValue(t *testing.T) {
	program := `
		pkgname=apackage
		pkgver=1.2.3
		builddir="$srcdir"/$pkgname-$pkgver
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckDefaultBuildDir,
		AL1,
		testEnv{},
	)
}

func TestDefaultBuilddirValueWithParamExpansion(t *testing.T) {
	program := `
		pkgname=apackage
		pkgver=1.2.3
		builddir="$srcdir"/$pkgname-${pkgver/-/_}
	`
	VariableTestCaseNoViolations(
		program,
		t,
		CheckDefaultBuildDir,
		testEnv{},
	)
}

func TestDefaultBuilddirValueWithReplacement(t *testing.T) {
	program := `
		pkgname=acccheck
		pkgver=1.2.3
		builddir="$srcdir/$pkgname-${pkgver//./-}"
	`
	VariableTestCaseNoViolations(
		program,
		t,
		CheckDefaultBuildDir,
		testEnv{},
	)
}

func TestDefaultBuilddirValueWithReplacement2(t *testing.T) {
	program := `
		pkgname=acccheck
		pkgver=1.2.3
		builddir="$srcdir/$pkgname-${pkgver/_/-}"
	`
	VariableTestCaseNoViolations(
		program,
		t,
		CheckDefaultBuildDir,
		testEnv{},
	)
}

func TestDefaultBuilddirValueWithPrefixCut(t *testing.T) {
	program := `
		pkgname=py3-traitlets
		pkgver=1.2.3
		builddir="$srcdir/${pkgname#py3-}-$pkgver"
	`
	VariableTestCaseNoViolations(
		program,
		t,
		CheckDefaultBuildDir,
		testEnv{},
	)
}

func TestPkgNameIsNotQuoted(t *testing.T) {
	program := `
		pkgname="apackage"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckPkgNameIsNotQuoted,
		AL3,
		testEnv{},
	)
}

func TestPkgVersionIsNotQuoted(t *testing.T) {
	program := `
		pkgver="1.2.3"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckPkgVersionIsNotQuoted,
		AL4,
		testEnv{},
	)
}

func TestGlobalVariableIsNotEmptyUnquoted(t *testing.T) {
	program := `
		makedepends=
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckGlobalVariableIsNotEmpty,
		AL5,
		testEnv{},
	)
}

func TestGlobalVariableIsNotEmptyQuoted(t *testing.T) {
	program := `
		makedepends=""
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckGlobalVariableIsNotEmpty,
		AL5,
		testEnv{},
	)
}

func TestGlobalVariableIsNotEmptyMultipleVariables(t *testing.T) {
	file := apkbuild.MakeFile(heredoc.Doc(`
		var1=""
		var2="value"
		var3=""
	`))

	apkbuild, err := apkbuild.Parse(file, "APKBUILD")
	require.NoError(t, err)

	violations := SortViolationsByLine(CheckGlobalVariableIsNotEmpty(apkbuild, testEnv{}.Getenv))

	require.Len(t, violations, 2)
	require.Contains(t, violations[0].String(), "var1", "description of violation 1 should contain 'var1'")
	require.Contains(t, violations[1].String(), "var3", "description of violation 1 should contain 'var3'")
}

func TestGlobalVariableIsNotEmptyIgnoresCustomVariables(t *testing.T) {
	program := heredoc.Doc(`
		_var1=""
	`)

	VariableTestCaseNoViolations(
		program,
		t,
		CheckGlobalVariableIsNotEmpty,
		testEnv{},
	)
}

func TestUseOfCustomVariables(t *testing.T) {
	program := `
		myvar=value
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckUseOfCustomVariables,
		AL6,
		testEnv{},
	)
}

func TestUseOfCustomVariablesInFunctionIsAllowed(t *testing.T) {
	program := `
		func() {
			myvar=value
		}
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckUseOfCustomVariables,
		testEnv{},
	)
}

func TestUseOfCustomVariableWithUnderscorePrefixIsAllowed(t *testing.T) {
	program := `
		_myvar=value
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckUseOfCustomVariables,
		testEnv{},
	)
}

func TestUseOfCustomVariableWithOfficialVariableIsAllowed(t *testing.T) {
	program := `
		pkgname=value
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckUseOfCustomVariables,
		testEnv{},
	)

}

func TestPkgNameUppercase(t *testing.T) {
	program := `
		pkgname=MyPackage
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckPkgNameUppercase,
		AL14,
		testEnv{},
	)
}

func TestPkgNameUppercaseAllowsLowercase(t *testing.T) {
	program := `
		pkgname=mypackage
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckPkgNameUppercase,
		testEnv{},
	)
}

func TestPkgRelInPkgVerWithDash(t *testing.T) {
	program := `
		pkgver=1.2-r1
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckPkgRelInPkgVer,
		AL15,
		testEnv{},
	)
}

func TestPkgRelInPkgVerWithUnderscore(t *testing.T) {
	program := `
		pkgver=1.2_r1
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckPkgRelInPkgVer,
		AL15,
		testEnv{},
	)
}

func TestPkgRelInPkgVerAllowsRC(t *testing.T) {
	program := `
		pkgver=1.2_rc1
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckPkgRelInPkgVer,
		testEnv{},
	)
}

func TestBackticksUsage(t *testing.T) {
	program := "JOBS=`nproc`"

	VariableTestCaseSingleViolation(program, t, CheckBackticksUsage, AL25, testEnv{})
}

func TestBackticksUsageInFunction(t *testing.T) {
	program := "func() {\n	JOBS=`nproc`\n}"

	VariableTestCaseSingleViolation(program, t, CheckBackticksUsage, AL25, testEnv{})
}

func TestBackticksUsageComplex(t *testing.T) {
	program := "foo=\"abc `cat foobar`\""

	VariableTestCaseSingleViolation(program, t, CheckBackticksUsage, AL25, testEnv{})
}

func TestUnderscoreBuildDir(t *testing.T) {
	program := `
		_builddir="$scrdir/foobar"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckUnderscoreBuildDir,
		AL26,
		testEnv{},
	)
}

func TestUnderscoreBuildDirWithBuildDir(t *testing.T) {
	program := `
		builddir="$scrdir/foobar"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckUnderscoreBuildDir,
		testEnv{},
	)
}

func TestLiteralIntegerIsQuotedDoubleQuotes(t *testing.T) {
	program := `
		foobar="1"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckLiteralIntegerIsQuoted,
		AL28,
		testEnv{},
	)
}

func TestLiteralIntegerIsQuotedSingleQuotes(t *testing.T) {
	program := `
		foobar='1'
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckLiteralIntegerIsQuoted,
		AL28,
		testEnv{},
	)
}

func TestLiteralIntegerIsQuotedWithSpacesIsFine(t *testing.T) {
	program := `
		foobar="1 2"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckLiteralIntegerIsQuoted,
		testEnv{},
	)
}

func TestLiteralIntegerIsQuotedQuotesInQuotesIsFine(t *testing.T) {
	program := `
		foobar='"1"'
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckLiteralIntegerIsQuoted,
		testEnv{},
	)
}

func TestLiteralIntegerIsQuotedUnquotedIsFine(t *testing.T) {
	program := `
		foobar=1
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckLiteralIntegerIsQuoted,
		testEnv{},
	)
}

func TestPkgnameInSource(t *testing.T) {
	program := `
		source="$pkgname-$pkver.tar.gz::https://example.com/$pkgname-$pkgver.tar.gz"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckPkgnameInSource,
		AL29,
		testEnv{},
	)
}

func TestPkgnameInSourceWithPkgNameOnlyInArchiveName(t *testing.T) {
	program := `
		source="$pkgname-$pkver.tar.gz::https://example.com/foobar-$pkgver.tar.gz"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckPkgnameInSource,
		testEnv{},
	)
}

func TestDoubleUnderOnVariables(t *testing.T) {
	program := `
		__myvar=val
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckDoubleUnderOnVariables,
		AL30,
		testEnv{},
	)
}

func TestDoubleUnderOnVariablesInFunction(t *testing.T) {
	t.Skip("Not yet implemented")
	program := `
		foo() {
			local __myvar=val
		}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckDoubleUnderOnVariables,
		AL30,
		testEnv{},
	)
}

func TestVariableCapitalized(t *testing.T) {
	program := `
		MyVar=val
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckVariableCapitalized,
		AL31,
		testEnv{},
	)
}

func TestVariableCapitalizedInFunction(t *testing.T) {
	program := `
		foo() {
			MyVar=val
 		}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckVariableCapitalized,
		AL31,
		testEnv{},
	)
}

func TestVariableCapitalizedIgnoresLowercaseVariables(t *testing.T) {
	program := `
		myvar=val
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckVariableCapitalized,
		testEnv{},
	)
}

func TestUnnecessaryUseOfBraces(t *testing.T) {
	program := `
		myvar="abc/${var}/def"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckUnnecessaryUseOfBraces,
		AL32,
		testEnv{},
	)
}

func TestUnnecessaryUseOfBracesWithValidBraces(t *testing.T) {
	t.Run("SucceedingLetters", func(t *testing.T) {
		program := `myvar="abc/${var}def"`
		VariableTestCaseNoViolations(program, t, CheckUnnecessaryUseOfBraces, testEnv{})
	})
	t.Run("SucceedingNumbers", func(t *testing.T) {
		program := `myvar="abc/${var}1"`
		VariableTestCaseNoViolations(program, t, CheckUnnecessaryUseOfBraces, testEnv{})
	})
	t.Run("ParameterSubstitution", func(t *testing.T) {
		program := `myvar="abc/${var%%.*}-def"`
		VariableTestCaseNoViolations(program, t, CheckUnnecessaryUseOfBraces, testEnv{})
	})
	t.Run("Replacement", func(t *testing.T) {
		program := `myvar="abc/${var/a/b}-def"`
		VariableTestCaseNoViolations(program, t, CheckUnnecessaryUseOfBraces, testEnv{})
	})
	t.Run("Slices", func(t *testing.T) {
		program := `myvar="abc/${var:1:2}-def"`
		VariableTestCaseNoViolations(program, t, CheckUnnecessaryUseOfBraces, testEnv{})
	})
}

func TestUnnecessaryUseOfBracesWithoutBraces(t *testing.T) {
	program := `
		myvar="abc/$var-def"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckUnnecessaryUseOfBraces,
		testEnv{},
	)
}

func TestUnnecessaryUseOfBracesWithLiteralAfterQuotes(t *testing.T) {
	t.Skip("Not imlemented")
	program := `
		myvar="abc/${def}"/ghi
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckUnnecessaryUseOfBraces,
		AL32,
		testEnv{},
	)
}

func TestUnnecessaryUseOfBracesWithVariableInFunction(t *testing.T) {
	program := `
		foo() {
			myvar="abc/${def}/ghi"
		}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckUnnecessaryUseOfBraces,
		AL32,
		testEnv{},
	)
}

func TestUnnecessaryUseOfBracesWithinSingleQuotes(t *testing.T) {
	program := `
		myvar='abc/${var}/def'
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckUnnecessaryUseOfBraces,
		testEnv{},
	)
}

func TestCpanVariablesDepends(t *testing.T) {
	program := `
		cpandepends="foo"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckCpanVariables,
		AL35,
		testEnv{},
	)
}

func TestCpanVariablesMakeDepends(t *testing.T) {
	program := `
		cpanmakedepends="foo"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckCpanVariables,
		AL35,
		testEnv{},
	)
}

func TestCpanVariablesCheckDepends(t *testing.T) {
	program := `
		cpancheckdepends="foo"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckCpanVariables,
		AL35,
		testEnv{},
	)
}

func TestOverwriteXflags(t *testing.T) {
	for _, name := range []string{"CFLAGS", "CPPFLAGS", "CXXFLAGS", "GOFLAGS"} {
		program := fmt.Sprintf(`
			export %s="abc"
		`, name)

		VariableTestCaseSingleViolation(
			program,
			t,
			CheckOverwriteXflags,
			AL36,
			testEnv{},
		)
	}
}

func TestOverwriteXflagsInsideFunction(t *testing.T) {
	for _, name := range []string{"CFLAGS", "CPPFLAGS", "CXXFLAGS", "GOFLAGS"} {
		program := fmt.Sprintf(`
			func() {
				export %s="abc"
			}
		`, name)

		VariableTestCaseSingleViolation(
			program,
			t,
			CheckOverwriteXflags,
			AL36,
			testEnv{},
		)
	}
}

func TestOverwriteXflagsCorrectlyDetectsVariable(t *testing.T) {
	program := `
		CFLAGS="$CFLAGSABC foo"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckOverwriteXflags,
		AL36,
		testEnv{},
	)
}

func TestOverwriteXflagsCorrectlyDetectsMultipleAssignments(t *testing.T) {
	program := `
		CFLAGS="foo"
		CFLAGS="$CFLAGS bar"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckOverwriteXflags,
		AL36,
		testEnv{},
	)
}

func TestOverwriteXflagsCorrect(t *testing.T) {
	for _, name := range []string{"CFLAGS", "CPPFLAGS", "CXXFLAGS", "GOFLAGS"} {
		program := fmt.Sprintf(`
			export %[1]s="$%[1]s abc"
		`, name)

		VariableTestCaseNoViolations(
			program,
			t,
			CheckOverwriteXflags,
			testEnv{},
		)

	}
}

func TestOverwriteXflagsCorrectSubstitution(t *testing.T) {
	program := `
		export CFLAGS="${CFLAGS/-g/} -o2"
	`
	VariableTestCaseNoViolations(
		program,
		t,
		CheckOverwriteXflags,
		testEnv{},
	)
}

func TestOverwriteXflagsExceptPerl(t *testing.T) {
	program := `export CFLAGS=$(perl -MConfig -E 'say $Config{ccflags}')`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckOverwriteXflags,
		testEnv{},
	)
}

func TestInvalidOption(t *testing.T) {
	program := `
		options="invalid"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckInvalidOption,
		AL49,
		testEnv{},
	)
}

func TestInvalidOptionValid(t *testing.T) {
	program := `
		options="net"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckInvalidOption,
		testEnv{},
	)
}

func TestInvalidOptionCustomOverride(t *testing.T) {
	program := `
		options="pfx:custom"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckInvalidOption,
		testEnv{
			"CUSTOM_VALID_OPTIONS": "pfx:custom",
		},
	)
}

func TestInvalidArch(t *testing.T) {
	program := `
		arch="invalid"
	`

	VariableTestCaseSingleViolation(program, t, CheckInvalidArch, AL57, testEnv{})
}

func TestInvalidArchValidAll(t *testing.T) {
	program := `
		arch="all"
	`

	VariableTestCaseNoViolations(program, t, CheckInvalidArch, testEnv{})
}

func TestInvalidArchValidNoarch(t *testing.T) {
	program := `
		arch="noarch"
	`

	VariableTestCaseNoViolations(program, t, CheckInvalidArch, testEnv{})
}

func TestInvalidArchValidArch(t *testing.T) {
	program := `
		arch="x86_64"
	`

	VariableTestCaseNoViolations(program, t, CheckInvalidArch, testEnv{})
}

func TestInvalidArchValidCombination(t *testing.T) {
	program := `
		arch="x86_64 x86"
	`

	VariableTestCaseNoViolations(program, t, CheckInvalidArch, testEnv{})
}

func TestInvalidArchValidArchNegated(t *testing.T) {
	program := `
		arch="all !riscv64"
	`

	VariableTestCaseNoViolations(program, t, CheckInvalidArch, testEnv{})
}

func TestInvalidOptionValidNegatedOption(t *testing.T) {
	program := `
		options="!check"
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckInvalidOption,
		testEnv{},
	)
}

func TestInvalidOptionMixed(t *testing.T) {
	program := `
		options="net invalid"
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckInvalidOption,
		AL49,
		testEnv{},
	)
}

func TestBadVersion(t *testing.T) {
	program := `
		pkgver=invalid
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckBadVersion,
		AL61,
		testEnv{},
	)
}

func TestBadVersionWithValidVersion(t *testing.T) {
	program := `
		pkgver=1.2.3
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckBadVersion,
		testEnv{},
	)
}

func TestVolatileSource(t *testing.T) {
	program := `
		source="https://github.com/example/project/commit/0123456789abcdef01234567890abcdef12345678.patch"
	`
	VariableTestCaseSingleViolation(
		program,
		t,
		CheckVolatileSource,
		AL62,
		testEnv{},
	)
}

func TestVolatileSourceRenamedPatch(t *testing.T) {
	program := `
		source="fix-build-issue.patch::https://github.com/example/project/commit/0123456789abcdef01234567890abcdef12345678.patch"
	`
	VariableTestCaseSingleViolation(
		program,
		t,
		CheckVolatileSource,
		AL62,
		testEnv{},
	)
}

func TestVolatileSourceMultipleSources(t *testing.T) {
	program := `
		source="
			https://github.com/example/project/archive/v1.2.3/archive-v1.2.3.tar.gz
			https://github.com/example/project/commit/0123456789abcdef01234567890abcdef12345678.patch"
	`
	VariableTestCaseSingleViolation(
		program,
		t,
		CheckVolatileSource,
		AL62,
		testEnv{},
	)
}

func TestVolatileSourceNormalSource(t *testing.T) {
	program := `
		source="https://github.com/example/project/archive/v1.2.3/archive-v1.2.3.tar.gz"
	`
	VariableTestCaseNoViolations(
		program,
		t,
		CheckVolatileSource,
		testEnv{},
	)
}

func TestVolatileSourceLocalPatch(t *testing.T) {
	program := `
		source="fix-build-issue.patch"
	`
	VariableTestCaseNoViolations(
		program,
		t,
		CheckVolatileSource,
		testEnv{},
	)
}
