package linter

type testEnv map[string]string

func (te testEnv) Getenv(env string) string {
	if v, ok := te[env]; ok {
		return v
	} else {
		return ""
	}
}
