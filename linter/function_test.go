package linter

import (
	"testing"

	"alpinelinux.org/go/atools/apkbuild"
	"github.com/MakeNowJust/heredoc/v2"
	"github.com/stretchr/testify/require"
)

func FunctionTestCaseSingleViolation(
	testcase string,
	t *testing.T,
	checkFunc func(*apkbuild.Apkbuild, func(string) string) []LintingViolation,
	expectedViolation LintingViolation,
	env testEnv,
) {
	t.Helper()

	file := apkbuild.MakeFile(heredoc.Doc(testcase))

	apkbuild, err := apkbuild.Parse(file, "APKBUILD")
	require.NoError(t, err)

	violations := checkFunc(apkbuild, env.Getenv)

	require.NotEmpty(t, violations, "there should at least be one violation")
	require.True(t, violations[0].Is(expectedViolation), "expected violation %s, got %s", expectedViolation.Code, violations[0].Code)
}

func FunctionTestCaseNoViolations(
	testcase string,
	t *testing.T,
	checkFunc func(*apkbuild.Apkbuild, func(string) string) []LintingViolation,
	env testEnv,
) {
	t.Helper()

	file := apkbuild.MakeFile(heredoc.Doc(testcase))

	apkbuild, err := apkbuild.Parse(file, "APKBUILD")
	require.NoError(t, err)

	violations := checkFunc(apkbuild, env.Getenv)

	require.Empty(t, violations, "unexpected violations received: %#v", violations)
}

func TestReturnOne(t *testing.T) {
	program := `
	func() {
		make || return 1
	}
	`

	FunctionTestCaseSingleViolation(program, t, CheckUnnecessaryReturnOne, AL2, testEnv{})
}

func TestReturnOneDoesNotWarnOnReturnZero(t *testing.T) {
	program := `
	func() {
		make || return 0
	}
	`

	FunctionTestCaseNoViolations(program, t, CheckUnnecessaryReturnOne, testEnv{})
}

func TestReturnOneDoesNotWarnOnNormalCommand(t *testing.T) {
	program := `
	func() {
		make
	}
	`

	FunctionTestCaseNoViolations(program, t, CheckUnnecessaryReturnOne, testEnv{})
}

func TestReturnOneCompound(t *testing.T) {
	program := `
	func() {
		make && make check || return 1
	}
	`

	FunctionTestCaseSingleViolation(program, t, CheckUnnecessaryReturnOne, AL2, testEnv{})
}

func TestReturnOneBraces(t *testing.T) {
	program := `
	func() {
		make || { echo "oops"; return 1; }
	}
	`

	FunctionTestCaseNoViolations(program, t, CheckUnnecessaryReturnOne, testEnv{})
}

func TestFunctionsKeyword(t *testing.T) {
	program := `
	function a() {
		true
	}
	`

	FunctionTestCaseSingleViolation(program, t, CheckFunctionKeyword, AL9, testEnv{})
}

func TestFunctionsKeywordNormalFunction(t *testing.T) {
	program := `
	a() {
		true
	}
	`

	FunctionTestCaseNoViolations(program, t, CheckFunctionKeyword, testEnv{})
}

func TestSpaceBeforeFunctionParenthesis(t *testing.T) {
	program := `
	func () {
		true
	}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckSpaceBeforeFunctionParenthesis,
		AL10,
		testEnv{},
	)
}

func TestSpaceBeforeFunctionParenthesisDoesNotMatchCorrectStyle(t *testing.T) {
	program := `
	func() {
		true
	}
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckSpaceBeforeFunctionParenthesis,
		testEnv{},
	)
}

func TestSpaceAfterFunctionParenthesis(t *testing.T) {
	program := `
	func(){
		true
	}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckSpaceAfterFunctionParenthesis,
		AL11,
		testEnv{},
	)
}

func TestSpaceAfterFunctionParenthesisDoubleSpace(t *testing.T) {
	program := `
	func()  {
		true
	}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckSpaceAfterFunctionParenthesis,
		AL11,
		testEnv{},
	)
}

func TestSpaceAfterFunctionParenthesisSkipsSingleLineFunction(t *testing.T) {
	program := `
	func()   { true; }
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckSpaceAfterFunctionParenthesis,
		testEnv{},
	)
}

func TestOpeningNewlineBrace(t *testing.T) {
	program := `
	func()
	{
		true
	}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckNewlineOpeningBrace,
		AL12,
		testEnv{},
	)
}

func TestOpeningNewlineBraceCorrect(t *testing.T) {
	program := `
	func() {
		true
	}
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckNewlineOpeningBrace,
		testEnv{},
	)
}

func TestCheckSuperfluousCDBuilddir(t *testing.T) {
	program := `
	prepare() {
		cd "$builddir"
		make
	}
	`
	VariableTestCaseSingleViolation(
		program,
		t,
		CheckSuperfluousCDBuilddir,
		AL13,
		testEnv{},
	)
}

func TestMissingDefaultPrepare(t *testing.T) {
	program := `
	prepare() {
		true
	}
	`

	VariableTestCaseSingleViolation(
		program,
		t,
		CheckMissingDefaultPrepare,
		AL54,
		testEnv{},
	)
}

func TestMissingDefaultPrepareWhenPresent(t *testing.T) {
	program := `
	prepare() {
		default_prepare
	}
	`

	VariableTestCaseNoViolations(
		program,
		t,
		CheckMissingDefaultPrepare,
		testEnv{},
	)
}
