exec >&2

find . -name '*.go' -exec redo-ifchange {} \;
redo-ifchange go.mod go.sum compile

./compile ./cmd/apkbuild-lint "$3"
