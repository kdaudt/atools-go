exec >&2

find . -name '*.go' -exec redo-ifchange {} \;

go test -v ./...
